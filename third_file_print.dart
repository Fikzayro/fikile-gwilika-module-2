///This shit was tiring, but here we go.
class SimpleClass {
  //We will start with our null variables
  String? appName;

  String? category;

  String? year;

  //I will just print everything about the app once.
  String get app => "$appName, under the $category, won in $year";

  //I was supposed to create a function for this but I will settle with a getter field, easier to retrive a clas mamber with it
  String get capitalize => '$appName';
}

void main() {
  //Instantiate our class
  SimpleClass classMember = new SimpleClass();

  //Define our class members
  classMember.appName = 'Ambani Africa';

  classMember.category = 'Best Gaming Solution';

  classMember.year = '2021';

  //Print everything about my app of choice
  print(classMember.app);

  //Capitalize the appName string then print it
  print(classMember.capitalize.toUpperCase());
}

///I'm fucking done, the code could be improved and I made some unnecessary choices but it all worked out fine...
